```Thorn 2020 - Undergraduate Thesis```

Developing population genomic simulations to estimate rnadom match probability

<b>CONTENTS OF THORN_2020<b>

1.  allGenTogether.R
- Contains the code that plots all 3 RMP plots at the three different generation levels together on one plot. requires results from popSimulation.R and plottingRMP.R.

2.  expressionPlots.R   
- Contains code that plots the allele loss over generations. Requires results from popSimulation.R and plottingRMP.R.

3.  findcRMP.R
- Contains the function that takes population frequencies and a specific genome from the population and finds the cRMP of that genome.

4.  findFreqs.R
- Contains the function that finds the allele frequencies for each loci in a population matrix.

5.  plottingRMP.R
- Contains script that plots the RMP for each SQL database (different generations and higher resolution SNPs). Requires simulationFunction.R

6.  populationSimulation_dc.R
- Contains a clone of populationSimulation.R that does not require hypredFounders.

7.  populationSimulation.R
- Contains the function that creates a population and simulates meiosis. Requires recombineFunction.R  

8.  recombineFunction.R
- Contains a wrapper function for the hypredRecombine function that simplifies use. 

9.  Shiny.R
- Contains the code for the Shiny app.

10. simulationFunctions.R
- Contains the 2 functions - one creates the population SQL tables and the other performs the random probability simulation on a randomly picked genome. Requires populationSimulation.R, findcRMP.R, findFreqs.R, and recombineFunction.R

11. startingPop.R
- Creates a random starting population based on user supplied minor allele frequencies.

```Jamieson 2020 - Undergraduate Thesis```

Estimating heritability of phenotypes in white-tailed deer (Odocoileus virginianus) using genomic relatedness matrices and mixed modelling

<b>CONTENTS OF JAMIESON_2020<b>

1.  Jamieson_GRM_h2.sh
- Bash script used for generating GRMs and esimtating h2

```Tamara Newell-Bell 2019 - Undergraduate Thesis```

Determining time since deposition of passive drip stains by analyzing the colour of the bloodstain colour and the amount of DNA

<b>CONTENTS OF NEWELL-BELL_2019<b>

1.  R Script for Data analysis
- This R script includes all the analysis that was done AFTER the 4 final variables have been picked. The final_data matrix is inputted, but only the 4 variables were programmed to be used. From there, PC's are made, linear regressions, graphs and summary statistics that will explain and visualize everything you should need to know about the 4 variables and their associations, as well as the PCs.
 
2. Final data matrix
- This matrix is the text version of the updated matrix. If you are intending to run scripts in R, use this file because it is already formatted to work for R. It includes the same information as the data matrix; 110 samples, 27 variables (etc), it is just formatted for use in R. All the data I have ever collected is here, but formatted for use in R.
 
3. Raw data matrix 
- This is the csv version of the data. It includes 110 samples, 27 variables, etc. It is formatted in such a way that it is easier to read and interpret than the text matrix file (it includes headings). It separates the variables depending on if they are associated with bloodstain colour or the amount of DNA. Essentially, all the data that I have ever collected is here and in the easiest format to read.

```Rebecca Ridings 2019 - Undergraduate Thesis```

Quantifying secondary DNA transfer under different types of contact

<b>CONTENTS OF RIDINGS_2019<b>

1. Ridings_2019_thesis.R
- All thesis analyses

2. Ridings_2019_success_data
- Text file with sample name and quantities for all qPCR replicate samples 

3. Ridings_2019_transfer_data
- Text file with transfer success, binary value (success or failure) for each qPCR replicate sample. Success is based on whether the qPCR gave a quantity for that replicate or not

4. Ridings_2019_concentration_data
- text file with mean quantity of the qPCR replicates 
